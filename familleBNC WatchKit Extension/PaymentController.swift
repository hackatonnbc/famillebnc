//
//  ContactController.swift
//  familleBNC
//
//  Created by user on 2015-06-12.
//  Copyright (c) 2015 user. All rights reserved.
//

import WatchKit
import Foundation

protocol Context {
    typealias DelType
    typealias ObjType
    var delegate: DelType? { get set }
    var object: ObjType? { get set }
}

class PaymentController: WKInterfaceController{
    
    @IBOutlet weak var txtAmount: WKInterfaceLabel!
    var amount = 0;
    
    override func awakeWithContext(context: AnyObject?) {

        println("PaymentController::awakeWithContext")
        
        super.awakeWithContext(context)
        updateAmount()
        
        // Get contact from previous controller
        // var contact: Contact = (context as? Contact)!
        
        // println("contactId : \(contact.id), contactName : \(contact.name)")
        
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    @IBAction func subTen() {
        amount -= 10
        updateAmount()
        NSLog("", "Add one")
    }

    @IBAction func addTen() {
        amount += 10
        updateAmount()
        NSLog("", "Add ten")
    }
    
    func updateAmount () {
        txtAmount.setText("\(amount)$")
    }
}