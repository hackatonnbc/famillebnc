//
//  InterfaceController.swift
//  familleBNC WatchKit Extension
//
//  Created by user on 2015-06-12.
//  Copyright (c) 2015 user. All rights reserved.
//

import WatchKit
import Foundation

class InterfaceController: WKInterfaceController {
  
    /*
    * Mock Data
    */
    var contacts = [
        Contact(id: 0, name: "Max", loggedIn: false),
        Contact(id: 1, name: "Jenny", loggedIn: true),
        Contact(id: 2, name: "Dad", loggedIn: false),
        Contact(id: 3, name: "Oncle Joe", loggedIn: false),
        Contact(id: 4, name: "Mom", loggedIn: false)
    ]
    
    @IBOutlet weak var contactName: WKInterfaceButton!
    @IBOutlet weak var txtAmount: WKInterfaceLabel!
    @IBOutlet weak var contactImageBtn: WKInterfaceButton!
    @IBOutlet weak var chooseContactLabel: WKInterfaceLabel!

    var contactId: Int = 0

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
        chooseContactLabel.setText(NSLocalizedString("choose_contact", comment: "choose_contact"))
        contactName.setTitle(contacts[contactId].name)
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    @IBAction func prevContact() {
        
        contactId--
        
        // first contact, cant go back
        if (contactId < 0) {
            contactId = contacts.count-1
        } else {
            if (contacts[contactId].loggedIn == true) {
                prevContact()
            }
        }
        
        updateContact()
    }
    
    @IBAction func nextContact() {
        
        contactId++
        
        // last contact, cant go any further
        if (contactId >= (contacts.count)) {
            contactId = 0
        } else {
            if (contacts[contactId].loggedIn == true) {
                nextContact()
            }
        }
        
        updateContact()
    }
    
    func updateContact () {
        
        println("Current contact: \(contactId)")
        
        if (contactId < 0 || contactId > 4) {
            return
        }
        
        contactImageBtn.setBackgroundImageNamed("face_\(contactId)")
        contactName.setTitle(contacts[contactId].name)
    }
    
    @IBAction func contactSelected() {
        pushControllerWithName("PaymentController", context: contacts[contactId])
    }
}
