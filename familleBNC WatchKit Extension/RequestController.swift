//
//  RequestController.swift
//  familleBNC
//
//  Created by user on 2015-06-12.
//  Copyright (c) 2015 user. All rights reserved.
//

import WatchKit
import Foundation

class RequestController: WKInterfaceController {
    
    @IBOutlet weak var requestWantsLabel: WKInterfaceLabel!
    @IBOutlet weak var acceptLabel: WKInterfaceLabel!
    @IBOutlet weak var declineLabel: WKInterfaceLabel!
    
    override func awakeWithContext(context: AnyObject?) {
        
        super.awakeWithContext(context)
        requestWantsLabel.setText(NSLocalizedString("give",comment:"give") + " 60$ " +
           NSLocalizedString("to",comment:"to") + " Jenny")
        acceptLabel.setText(NSLocalizedString("accept",comment:"accept"))
        declineLabel.setText(NSLocalizedString("decline",comment:"decline"))
        
        // Get contact from previous controller
        //var contact: Contact = (context as? Contact)!
        
        //println("contactId : \(contact.id), contactName : \(contact.name)")
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
}