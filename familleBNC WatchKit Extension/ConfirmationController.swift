//
//  ConfirmationController.swift
//  familleBNC
//
//  Created by user on 2015-06-12.
//  Copyright (c) 2015 user. All rights reserved.
//

import WatchKit
import Foundation

class ConfirmationController: WKInterfaceController {
    @IBOutlet weak var confirmationLabel: WKInterfaceLabel!
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
            NSLog("", "CONFIRMATION CONTROLLER")
            confirmationLabel.setText(NSLocalizedString("request_sent", comment: "Request sent"))
        }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}