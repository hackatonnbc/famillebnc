//
//  Contact.swift
//  familleBNC
//
//  Created by user on 2015-06-13.
//  Copyright (c) 2015 user. All rights reserved.
//

import Foundation

class Contact {
    var id = 0;
    var name = ""
    var loggedIn = false
    
    init(id: Int, name: String, loggedIn: Bool) {
        self.id = id
        self.name = name
        self.loggedIn = loggedIn
    }
}
